package wojtowic;

import java.util.List;
import javax.transaction.Transactional;


// Crud repository
import org.springframework.data.repository.Repository;

@Transactional
public interface JobRepository extends Repository<Job, Long>
{
	public Job save(Job job);
	public Iterable<Job> save(Iterable<Job> jobs);
	public void delete(Job job);
	public void deleteById(Long id);
	public void deleteByCipherId(Long cipherId);
	public long count();
	public Job findByIdIn(long l);
	public Job findTopByStartDateIsNullOrderByIdAsc();
	public Job findTopByOrderByStartDateAsc();
	public Job findTopByOrderByEndDesc();
	public List<Job> findAll();
	public List<Job> findByCipherId(Long cipherId);
}