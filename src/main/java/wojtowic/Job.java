package wojtowic;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="Jobs")
public class Job {
    
    public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Job() {
		startDate = null;
	}

	public Job(Long cipherId, long begin2, long l) {
		this.cipherId = cipherId;
		this.begin = begin2;
		this.end = l;
		startDate = null;
	}

	public Long getCipherId() {
		return cipherId;
	}

	public void setCipherId(Long cipherId) {
		this.cipherId = cipherId;
	}

	public Long getBegin() {
		return begin;
	}

	public void setBegin(Long begin) {
		this.begin = begin;
	}

	public Long getEnd() {
		return end;
	}

	public void setEnd(Long end) {
		this.end = end;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	
	public boolean equals(Job job){
		return this.begin.equals(job.begin) && 
				this.end.equals(job.end) &&
				this.startDate.equals(job.startDate) &&
				this.id.equals(job.id) &&
				this.cipherId.equals(job.cipherId);
	}

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="Id")
	private Long id;
	
    @Column(name="CipherId")
    @NotNull
	private Long cipherId;

	@Column(name="Begin")
    @NotNull
	private Long begin;

	@Column(name="End")
    @NotNull
	private Long end;

	@Column(name="StartDate")
	private String startDate;

}
