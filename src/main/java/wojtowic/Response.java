package wojtowic;

public class Response {
	
	public Response() {
		
	}
	
	public Response(Job job, Cipher cipher) {
		this.id = job.getId();
		this.cipherId = job.getCipherId();
		this.begin = job.getBegin();
		this.end = job.getEnd();
		this.startDate = job.getStartDate();
		this.hash = cipher.getHash();
		this.possibleCharacters = cipher.getPossibleCharacters();
	}
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCipherId() {
		return cipherId;
	}

	public void setCipherId(Long cipherId) {
		this.cipherId = cipherId;
	}

	public Long getBegin() {
		return begin;
	}

	public void setBegin(Long begin) {
		this.begin = begin;
	}

	public Long getEnd() {
		return end;
	}

	public void setEnd(Long end) {
		this.end = end;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getHash() {
		return hash;
	}

	public void setHash(String hash) {
		this.hash = hash;
	}

	public String getPossibleCharacters() {
		return possibleCharacters;
	}

	public void setPossibleCharacters(String possibleCharacters) {
		this.possibleCharacters = possibleCharacters;
	}


	private Long id;
	private Long cipherId;
	private Long begin;
	private Long end;
	private String startDate;
	private String hash;
	private String possibleCharacters;
}
