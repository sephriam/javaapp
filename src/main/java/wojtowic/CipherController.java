package wojtowic;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class CipherController {
	
	@Autowired
	private CipherRepository cipherRepository;
	
	@Autowired
	private UserRepository userRepository;

    private static final Long jobBaseSize = (long)10000000;

	@GetMapping("/addcipher")
    public String addCipherForm(@CookieValue(value = "userId", defaultValue = "-1") String userId, Model model) {
        User user = userRepository.findByIdIn(Integer.parseInt(userId));
        if(user == null){
            model.addAttribute("user", new User());
            return "login";   
        }
        model.addAttribute("points", user.getPoints());
        model.addAttribute("cipher", new Cipher());
        return "addcipher";
    }

    @PostMapping("/addcipher")
    public String addCipherSubmit(@CookieValue(value = "userId", defaultValue = "-1") String userId,
    		@ModelAttribute Cipher cipher, Model model) {
    	 if(userId.equals("-1")){
            model.addAttribute("user", new User());
            return "login";           
        }

        User user = userRepository.findByIdIn(Integer.parseInt(userId));

        if(user == null){
        	model.addAttribute("user", new User());
            return "login";   
        }

        model.addAttribute("points", user.getPoints());
        cipher.RemoveRedundantCharacters();
        if(!cipher.CheckAndAddRange()){
        	model.addAttribute("cipher", new Cipher());
        	model.addAttribute("error", "Cipher out of range!");
        	return "addcipher"; 
        }
        

        Long pointsReq = (cipher.getCombinations() / jobBaseSize);
        if(user.getPoints() >= pointsReq) {
        	user.setPoints(user.getPoints() - pointsReq);
        	userRepository.save(user);
        } else {
        	model.addAttribute("cipher", new Cipher());
        	model.addAttribute("error", "Not enough points!");
        	return "addcipher"; 
        }

        cipher.setHash(cipher.getHash().toUpperCase());
        cipher.setUserId(Integer.parseInt(userId));
    	cipherRepository.save(cipher);
    	
    	model.addAttribute("list", cipherRepository.findByUserId(Integer.valueOf(userId)));
    	return "ciphers";
    }

    @GetMapping("/ciphers")
    public String getCipherList(@CookieValue(value = "userId", defaultValue = "-1") String userId, Model model) {
        if(userId.equals("-1")){
            model.addAttribute("user", new User());
            return "login";           
        }
    	model.addAttribute("list", cipherRepository.findByUserId(Integer.valueOf(userId)));
    	return "ciphers";
    }

    @GetMapping("/delete")
    public String deleteCipher(@CookieValue(value = "userId", defaultValue = "-1") String userId,
                                @RequestParam("id") String cipherId, Model model) {

        if(userId.equals("-1")){
            model.addAttribute("user", new User());
            return "login";           
        }

        Cipher cipher = cipherRepository.findByIdIn(Long.parseLong(cipherId));
        if(cipher.getUserId() == Integer.parseInt(userId)) {
            cipherRepository.delete(cipher);
        }

        model.addAttribute("list", cipherRepository.findByUserId(Integer.parseInt(userId)));
        return "ciphers";
    }
    
}
