package wojtowic;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="Ciphers")
public class Cipher {
    
    public Long getCombinations() {
		return combinations;
	}

	public void setCombinations(Long combinations) {
		this.combinations = combinations;
	}
	
    public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDecoded() {
		return decoded;
	}

	public void setDecoded(String decoded) {
		this.decoded = decoded;
	}

	public String getHash() {
		return hash;
	}

	public Integer getMaxLength() {
		return maxLength;
	}

	public void setMaxLength(Integer maxLength) {
		this.maxLength = maxLength;
	}

	public void setHash(String hash) {
		this.hash = hash;
	}

	public String getPossibleCharacters() {
		return possibleCharacters;
	}

	public void setPossibleCharacters(String possibleCharacters) {
		this.possibleCharacters = possibleCharacters;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Long getJobsTaken() {
		return jobsTaken;
	}

	public void setJobsTaken(Long jobsTaken) {
		this.jobsTaken = jobsTaken;
	}

	public Long getJobsFinished() {
		return jobsFinished;
	}

	public void setJobsFinished(Long jobsFinished) {
		this.jobsFinished = jobsFinished;
	}

	public void RemoveRedundantCharacters() {
		
		Set<Character> uniquechars = new HashSet<Character>();
		
		for(int i = 0; i < possibleCharacters.length(); ++i) {
			uniquechars.add(possibleCharacters.charAt(i));
		}
		possibleCharacters = "";
		for(char it : uniquechars){
			possibleCharacters += it;
		}
	}
	
	public boolean CheckAndAddRange() {

		Long combinations = (long) 1;
		Long possibleCombinations = (long) 1;
		for(long i = 0; i < this.maxLength; ++i){
			try {
				combinations = Math.multiplyExact(combinations, possibleCharacters.length());
				possibleCombinations = Math.addExact(possibleCombinations, combinations);
			} catch (Exception e) {
				return false;
			}
		}
		
		this.combinations = possibleCombinations; 
		return true;
	}
	
	public Cipher() {
		
	}

	public Cipher(String hash, String possibleCharacters) {
		this.hash = hash;
		this.possibleCharacters = possibleCharacters;
		this.jobsTaken = (long)0;
		this.jobsFinished = (long)0;
	}

	@Id
    @GeneratedValue
    @Column(name="Id")
	private Long id;
	
    @Column(name="Hash")
    @NotNull
	private String hash;

	@Column(name="PossibleCharacters")
    @NotNull
	private String possibleCharacters;

	@Column(name="MaxLenght")
    @NotNull
	private Integer maxLength;	
	
    @Column(name="Decoded")
	private String decoded;
	
    @Column(name="UserId")
    @NotNull
	private Integer userId;

	@Column(name="Combinations")
    @NotNull
    private Long combinations;

	@Column(name="JobsTaken")
	@NotNull
    private Long jobsTaken = (long)0;

    @Column(name="JobsFinished")
    @NotNull
    private Long jobsFinished = (long)0;
}
