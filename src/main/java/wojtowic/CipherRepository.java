package wojtowic;

import java.util.List;
import javax.transaction.Transactional;

import org.springframework.data.repository.Repository;

@Transactional
public interface CipherRepository extends Repository<Cipher, Long>
{
	public Cipher save(Cipher cipher);
	public void delete(Cipher cipher);
	public long count();
	public Cipher findByIdIn(Long Id);
	public List<Cipher> findAll();
	public List<Cipher> findByUserId(Integer userId);
	public Cipher findTopByDecodedIsNullOrderByIdAsc();
}