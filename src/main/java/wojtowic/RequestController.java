package wojtowic;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RequestController {

	@Autowired
	JobRepository jobRepository;
	
	@Autowired
	CipherRepository cipherRepository;
	
	@Autowired
	UserRepository userRepository;
	
	private static final Long jobBaseSize = (long)10000000;
	
	@GetMapping("/getjob/{userId}/{jobSize}")
    public Response getBigJob(@PathVariable String userId, @PathVariable String jobSize) {
        
        if(userId == null || userId.equals("-1")) {
            return null;           
        }
        
        try {
            if(userRepository.findByIdIn(Integer.parseInt(userId)) == null) {
                return null;
            }
        } catch (Exception e) {
            return null;
        }

        Long jobClass = (long)1;
        try {
            jobClass = Math.multiplyExact(jobBaseSize, Long.parseLong(jobSize)) ;
        } catch (Exception e) {
        	return null;
        }
        
        if(jobClass <= 0) {
        	jobClass = jobBaseSize;
        }
        
        
        Job jobByDate = jobRepository.findTopByOrderByStartDateAsc();
        Job jobByEnd = jobRepository.findTopByOrderByEndDesc();
        Job job = jobByDate;
        
        DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmm");
        Date date = new Date();
        Long now = Long.valueOf(dateFormat.format(date));

        // No jobs at all
        if(jobByEnd == null) {
        	job = new Job();
        	Cipher cipher = cipherRepository.findTopByDecodedIsNullOrderByIdAsc();
        	if(cipher == null) {
        		return null;
        	}

            long beg = 0;
            long end = 0;
            try {
                beg = cipher.getJobsTaken();
                end = Math.addExact(beg, jobClass);
            } catch (Exception e) {
                return null;
            }

            if(end > cipher.getCombinations()) {
                end = cipher.getCombinations();
            }

            job.setBegin(beg);
        	job.setEnd(end);
        	job.setCipherId(cipher.getId());
        } else if ( Long.valueOf(now - Long.valueOf(jobByDate.getStartDate())).compareTo((long)2400) > 0) {
        	// found some old unfinished job started more than 24h ago
            job = jobByDate;
        } else {
        	job = new Job();
        	Cipher cipher = cipherRepository.findTopByDecodedIsNullOrderByIdAsc();
        	if(cipher == null) {
        		return null;
        	}
        	
        	long beg = 0;
        	long end = 0;
        	try {
                beg = cipher.getJobsTaken();
        		end = Math.addExact(beg, jobClass);
        	} catch (Exception e) {
        		return null;
        	}
        	if(end > cipher.getCombinations()) {
        		end = cipher.getCombinations();
        	}
        	
        	job.setBegin(beg);
        	job.setEnd(end);
        	job.setCipherId(cipher.getId());
        }
       
        if(job.getBegin().equals(job.getEnd())) {
            return null;
        }

        job.setStartDate(dateFormat.format(date));
        jobRepository.save(job);
        Cipher cipher = cipherRepository.findByIdIn(job.getCipherId());
        if(cipher == null) {
        	return null;
        }

        cipher.setJobsTaken(job.getEnd());
        if(cipher.getJobsTaken().equals(cipher.getCombinations())) {
            cipher.setJobsTaken(cipher.getCombinations());
            cipher.setDecoded("Waiting for jobs to be complete");
        }

        cipherRepository.save(cipher);


        Response ret = new Response(job,cipher);
        
        return ret;
    }
	
	@GetMapping(path = "/job/{userId}/{jobId}")
    public void postJob(@PathVariable String userId, @PathVariable String jobId) {
		
		User user = userRepository.findByIdIn(Integer.parseInt(userId));
        if(user == null) {
            return;           
        }
        Job job = jobRepository.findByIdIn(Long.parseLong(jobId));
        long jobSize = (job.getEnd() - job.getBegin()); 
        Cipher cipher = cipherRepository.findByIdIn(job.getCipherId());
        cipher.setJobsFinished(cipher.getJobsFinished() + jobSize);
        if(cipher.getJobsFinished() >= cipher.getCombinations()) {
            cipher.setDecoded("Cipher not found in specified range");
        }


        cipherRepository.save(cipher);

        long pointsToAdd = jobSize / jobBaseSize;;
        user.setPoints(user.getPoints() + pointsToAdd);
        
        
        if(jobId == null || jobId.equals("-1")) {
        	return;
        }

        jobRepository.deleteById(Long.parseLong(jobId));
        
        return;
    }
	
	@GetMapping(path = "/job/{userId}/{jobId}/{cipherId}/{decoded}")
    public void decoded(@PathVariable String userId, @PathVariable String jobId,
                        @PathVariable String cipherId, @PathVariable String decoded) {

        if(userId.equals("-1")) {
            return;           
        }
        
        if(cipherId == null || cipherId.equals("-1") ||
        	decoded == null || decoded.isEmpty()) {
        	return;
        }

        User user = userRepository.findByIdIn(Integer.parseInt(userId));
        if(user == null) {
            return;           
        }
        long pointsToAdd = 0;
        long jobClass = 0;
        try {
            Job job = jobRepository.findByIdIn(Long.parseLong(jobId));
            jobClass = job.getEnd() - job.getBegin();
            pointsToAdd = jobClass / jobBaseSize;
        } catch (Exception e) {
            return;
        }
        
        user.setPoints(user.getPoints() + pointsToAdd);

        try {
            jobRepository.deleteByCipherId(Long.parseLong(cipherId));
        } catch(Exception e) {
            return;
        }
        Cipher cipher = cipherRepository.findByIdIn(Long.parseLong(cipherId));
        
        if(cipher == null) {
        	return;
        }
        
        cipher.setJobsFinished(jobClass);
        cipher.setDecoded(decoded);
        cipherRepository.save(cipher);
        userRepository.save(user);
        
        return;
    }
	
	@PostMapping(path = "/checkuser")
	public Integer checkIfUserExists(@RequestParam("username") String userName,
			@RequestParam("password") String password) {
		
		User user = userRepository.findTopByUserNameInAndPasswordIn(userName, password);
		
		if(user != null) {
			return user.getId();
		}
		
		return -1;
	}
	
}
